import {Card, Button } from 'react-bootstrap'
import {useState} from 'react'
import {slotState} from 'react-bootstrap'
import Proptypes from 'prop-types'
import {Link} from 'react-router-dom'

export default function CourseCard({course}){
//Destructuring the props
  const {name, description, price, _id} = course

//Using the state -- Initialize count 0
  const [count, setCount] = useState(0)
  const [slots,setSlots] = useState(15)

/*function enroll(){
  if (slots > 0){
     setCount (count + 1)
  setSlots (slots -1)
 
 return
  }
 alert('Slots full')
};
*/

  return (
    <Card>
      <Card.Body>  
      <Card.Title>{name}</Card.Title>
      <Card.Text>{description}</Card.Text>
      <Card.Text>This is a sample course we are offering</Card.Text>
      <Card.Subtitle>
          Price:
      </Card.Subtitle>
      <Card.Text>PHP {price}</Card.Text>
      
      <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>

     </Card.Body>
    </Card>
  );
}

//Prop Types can be used to validate the data coming from the props. You can define each property of the props and assign specific VALIDATION for each of them.
CourseCard.propTypes = {
  course: Proptypes.shape({
      name: Proptypes.string.isRequired,
      description: Proptypes.string.isRequired,
      price: Proptypes.number.isRequired
  })
}