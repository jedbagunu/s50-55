const courses_data = [
	{
		id: "wdc001",
		name: "PHP and Laravel",
		description: "lorem ipsum dolor sit amet consectetur adipisicing elit. Animi natus eos, enim quas nihil deleniti ea nisi ipsam hic sed impedit laborum pariatur quo, quos? Illo, quisquam. Tempora explicabo, dignissimo",
		price: 4000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python Django",
		description: "lorem ipsum dolor sit amet consectetur adipisicing elit. Animi natus eos, enim quas nihil deleniti ea nisi ipsam hic sed impedit laborum pariatur quo, quos? Illo, quisquam. Tempora explicabo, dignissimo",
		price: 5000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java and Springboot",
		description: "lorem ipsum dolor sit amet consectetur adipisicing elit. Animi natus eos, enim quas nihil deleniti ea nisi ipsam hic sed impedit laborum pariatur quo, quos? Illo, quisquam. Tempora explicabo, dignissimo",
		price:7000,
		onOffer: true
	}

]

export default courses_data