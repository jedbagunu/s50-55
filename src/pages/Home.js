import '../App.css';
import AppNavbar from '../components/AppNavbar'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


export default function Home () {
	return (
		<>
		 <Banner/> 
         <Highlights/> 
		</>

	)
}
