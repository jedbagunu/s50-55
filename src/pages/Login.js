import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'



export default function Login(){

	//global access for user
	const {user, setUser} = useContext(UserContext)
	//global access user end

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	// Initialize useNavigate
	const navigate = useNavigate()

	// For determining if button is disabled or not
	const [isActive, setIsActive] = useState(false)

		const retrieveUser = (token)=>{
			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(response => response.json())
			.then(result =>{
				console.log(result)

				//stores the suer details retrieved from the token into the global state
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
			})
		}


	function authenticate(event){
		event.preventDefault()

		fetch (`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password

			})
		})
		.then(response => response.json())
		.then(result => {
			//stores token in localstorage if not undefined
			if(typeof result.accessToken !== "undefined") {
				localStorage.setItem('token', result.accessToken)

				retrieveUser(result.accessToken)

				Swal.fire({
					title: 'Login Successfully',
					icon: 'success',
					text:'Welcome to Zuitt'
				}) 
			}  else {
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'Error',
					text:'Try again'
				}) 

			}

		})

	}



	useEffect(() => {
		if((email !== '' && password !== '')){
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(
		
		(user.id !== null) ?
				<Navigate to="/courses"/>
			:
				<Form onSubmit={event => authenticate(event)}>
	        	<Form.Group controlId="userEmail">
	            <Form.Label>Email address</Form.Label>
	            <Form.Control 
	                type="email" 
	                placeholder="Enter email"
	                value={email}
	                onChange={event => setEmail(event.target.value)}
	                required
	            />
	            <Form.Text className="text-muted">
	                We'll never share your email with anyone else.
	            </Form.Text>
	        	</Form.Group>

	       		 <Form.Group controlId="password">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password"
	                value={password}
	                onChange={event => setPassword(event.target.value)}
	                required
	            />
	        	</Form.Group>

	       	 	{	isActive ?
	        	<Button variant="primary" type="submit" id="submitBtn">
		        	Submit
		        </Button>
		        :
		        <Button variant="primary" type="submit" id="submitBtn" disabled>
		        	Submit
		        </Button>
	       	 }
	        
	    		</Form>
			
	)
}
